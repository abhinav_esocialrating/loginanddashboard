import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux'
import { createStore, compose } from 'redux'
import MainReducers from './Reducers/MainReducers';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import OtpVerification from './Components/OtpVerification';
import Dashboard from './Components/Dashboard';
// import { makeStyles } from "@material-ui/core/styles";


const store = compose(window.devToolsExtension ? window.devToolsExtension() : f => f)(createStore)(MainReducers);


ReactDOM.render(<Provider store={store}>
    <App/>
        {/* <Router>
            <Route exact path="/" component={App} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/" component={App} />
        </Router> */}
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
