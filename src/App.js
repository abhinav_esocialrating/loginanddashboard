import React from 'react';
import LoginContainer from './Components/LoginContainer';
import './App.css';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import OtpVerification from './Components/OtpVerification';
import Dashboard from './Components/Dashboard';

class App extends React.Component {

  
  
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <p>
              <b>Trustze</b>
            </p>
          </header>
          <Switch>
            <Route path="/" exact component={LoginContainer}></Route>
            <Route path="/otpverification" component={OtpVerification}></Route>
            <Route path="/dashboard" component={Dashboard}></Route>
          </Switch>
          {/* <LoginContainer /> */}
      </div>
     </Router>
    )
  }
}



export default App;
