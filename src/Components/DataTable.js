import React, { Component } from 'react'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

export default class DataTable extends Component {
    render() {
        return (
            <div>
                <Table>
                <TableHead>
                <TableRow>
            <TableCell>Dessert (100g serving)</TableCell>
            <TableCell numeric>Calories</TableCell>
            <TableCell numeric>Fat (g)</TableCell>
            <TableCell numeric>Carbs (g)</TableCell>
            <TableCell numeric>Protein (g)</TableCell>
            
          </TableRow>
        </TableHead>
        <TableBody>
          
              <TableRow >
                <TableCell component="th" scope="row">
                  Froyo
                </TableCell>
                <TableCell numeric>159</TableCell>
                <TableCell numeric>4</TableCell>
                <TableCell numeric>24</TableCell>
                <TableCell numeric>4</TableCell>
                
              </TableRow>
            
        </TableBody>
                </Table>
            </div>
        )
    }
}
