import React, { Component } from 'react'
import {push} from 'react-router-dom';
import {withRouter, Link} from 'react-router-dom';
import { Redirect } from 'react-router-dom';
import { kBaseUrl, kUrlSignUp } from '../utils/AppUrls';

export default class LoginContainer extends Component {
    constructor(props){
        super(props)
        this.state = {
            phoneNumber: '',
        }
        this.onChangePhoneNumber = this.onChangePhoneNumber.bind(this)
      }
      onChangePhoneNumber(e){
        this.setState({
            phoneNumber: e.target.value
        })
      }
      async onClickLogin(e) {
        // Call search api
        console.log('onClickLogin');
        //this.callSignUpApiAsync.bind(this);
        var result= await this.callSignUpAPI();
        console.log("result:",result);
        if (result.status == 200) {
            console.log(result.data);
            this.moveToVerifyOTP.bind(this);
        } else {
            console.log(result.message);
        }
      }
    moveToVerifyOTP(e) {
        let { history } = this.props;
        history.push({
         pathname: '/otpverification'
        })
    }
    async callSignUpAPI(e) {
    console.log('callSignUpAPI');
    try {
        let apiData = {
            'number': this.state.phoneNumber,
            'countryCode': '+91',
          }
        //   console.log(JSON.stringify(apiData))
          let url = kBaseUrl + kUrlSignUp
          let resp = await fetch(url, {
            method: 'POST', headers: {
                  'Content-Type': 'application/json',
                  'device-id':'abcdsjfndsfn',
                  'device-type':'android'
          },
          body: JSON.stringify(apiData)
          });
          let json = await resp.json();
          console.log(json)
          return json
       } catch (error) {
           console.log("catch:",error);
           return error;
       }
   }

    getStyle = () => {
        return {
            backgroundColor: '#FAF9F8',
            marginLeft: 'auto',
            marginRight: 'auto',
            marginTop: '100px',
            height: '300px',
            width: '300px',
            boxShadow: '0px 0px 5px #9E9E9E',
            borderRadius: '10px',
            overflow: 'hidden'
        }
    }
    

    render() {
        return (
            
            <div style={ this.getStyle() }>
                <div style={ styles.boxTitle }>
                    <p style={ styles.signInLabel }>Sign In</p>
                </div>
                
                    <div style={ styles.textDivs }>
                        <input onChange={this.onChangePhoneNumber} style={ styles.inputField } name="username" placeholder="Phone Number"/><br/>
                    </div>
                    <button onClick={ this.onClickLogin.bind(this) } style={ styles.submitBtn }>Submit</button>
                
            </div>
        )
    }
}

const styles = {

    boxTitle: {
        width: 'auto',
        height: '50px',
        backgroundColor: '#3E88EC',
        
    },
    signInLabel: {
        margin: 'auto',
        lineHeight: '50px',
        color: '#FFFFFF'
    },
    textDivs: {
        marginTop: '50px',
        marginRight: '10px',
        marginLeft: '10px',
        padding: '5px',
        backgroundColor: '#E6E6E6',
        height: '30px',
        borderRadius: '20px'
    },
    inputField: {
        width: '90%',
        height: '90%',
        backgroundColor: '#E6E6E6',
        border: 'none',
        
    },
    submitBtn: {
        marginTop: '30px',
        marginRight: '10px',
        marginLeft: '10px',
        height: '40px',
        backgroundColor: '#3E88EC',
        color: '#FFFFFF',
        border: 'none',
        width: '90%',
        borderRadius: '20px'
    }

}