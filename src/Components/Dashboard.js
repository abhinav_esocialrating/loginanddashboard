import React, { Component } from 'react'
// import Grid from '@material/core/Grid';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'
import GridList from '@material-ui/core/GridList'
import Container from '@material-ui/core/Container'
import Card from '@material-ui/core/Card'
import DataTable from './DataTable';
import Donut from '../Charts/Donut';
import PieChart from '../Charts/PieChart';

export default class Dashboard extends Component {
    render() {
        return (
            <div>
                
                <GridList cellHeight={100} style={{
                    marginTop: '10px',
                    width: '100%',
                    
                }}>
                    <Grid container item sm={2} direction="column"  style={{height: '200px', margin: 'auto'}}>
                        <Card style={{height: '100%'}}>
                            <Donut/>
                        </Card>
                    </Grid>
                    <Grid container item sm={2} direction="column"  style={{height: '200px', margin: 'auto'}}>
                        <Card style={{height: '100%'}}>
                            <PieChart/>
                        </Card>
                    </Grid>
                    <Grid container item sm={2} direction="column"  style={{height: '200px', margin: 'auto'}}>
                        <Card style={{height: '100%'}}>
                            <p> Siddharth</p>
                        </Card>
                    </Grid>
                    <Grid container item sm={2} direction="column"  style={{height: '200px', margin: 'auto'}}>
                        <Card style={{height: '100%'}}>
                            <p>Manan</p>
                        </Card>
                    </Grid>
                </GridList>
                <DataTable/>
            </div>
        )
    }
}

