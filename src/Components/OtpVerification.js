import React, { Component } from 'react'
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom'


export default class OtpVerification extends Component {

    constructor(props){
        super(props)
        this.state = {
            phoneNumber: '',
            otp:''
        }
        this.onChangeOTP = this.onChangeOTP.bind(this)
    }
    onChangeOTP(e){
    this.setState({
        phoneNumber: e.target.value
    })
    }
    getStyle = () => {
        return {
            backgroundColor: '#FAF9F8',
            marginLeft: 'auto',
            marginRight: 'auto',
            marginTop: '100px',
            height: '300px',
            width: '300px',
            boxShadow: '0px 0px 5px #9E9E9E',
            borderRadius: '10px',
            overflow: 'hidden'
        }
    }

    verifyOTP(e) {
        // <Redirect to="/dashboard"/>
        let { history } = this.props;
        history.push({
         pathname: '/dashboard',
        })
    }

    render() {
        return (
            <div style={ this.getStyle() }>
                <div style={ styles.boxTitle }>
                    <p style={ styles.signInLabel }>Verify OTP</p>
                </div>
                    <div style={ styles.textDivs}>
                    <span style={ styles.inlineBlocks}>
                        <input onChange={this.onChangeOTP} style={ styles.inputField } name="first" placeholder=" "/><br/>
                        <input onChange={this.onChangeOTP} style={ styles.inputField } name="second" placeholder=" "/><br/>
                        <input onChange={this.onChangeOTP} style={ styles.inputField } name="third" placeholder=" "/><br/>
                        <input onChange={this.onChangeOTP} style={ styles.inputField } name="fourth" placeholder=" "/><br/>
                    </span>
                    </div>
                    <button onClick={ this.verifyOTP.bind(this) } style={ styles.submitBtn }>Submit</button>
            </div>
        )
    }
}

const styles = {

    boxTitle: {
        width: 'auto',
        height: '50px',
        backgroundColor: '#3E88EC',
        
    },
    signInLabel: {
        margin: 'auto',
        lineHeight: '50px',
        color: '#FFFFFF'
    },
    textDivs: {
        marginTop: '50px',
        marginRight: '10px',
        marginLeft: '10px',
        padding: '5px',
        backgroundColor: '#E6E6E6',
        height: '30px',
        borderRadius: '20px',
        display: 'block',
    },
    inlineBlocks: {
       backgroundColor: 'red',
        display: 'flex',
        margin: '0 auto'
    },
    inputField: {
        width: '30px',
        height: '30px',
        backgroundColor: '#E6E6E6',
        border: 'dotted',
    },
    submitBtn: {
        marginTop: '30px',
        marginRight: '10px',
        marginLeft: '10px',
        height: '40px',
        backgroundColor: '#3E88EC',
        color: '#FFFFFF',
        border: 'none',
        width: '90%',
        borderRadius: '20px'
    }
}
