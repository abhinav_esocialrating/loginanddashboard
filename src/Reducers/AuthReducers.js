import {
    AUTH
} from "../actions/actionTypes";

const AuthData = ''

const AuthReducer = (state = AuthData, action) => {
    switch (action.type) {
      case AUTH:          
        return action.text;
      default:
        return state;
    }
};

export default AuthReducer;