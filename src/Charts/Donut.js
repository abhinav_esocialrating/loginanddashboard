import React, { Component } from 'react'
import {Doughnut} from 'react-chartjs-2'

export default class Donut extends Component {
    render() {
        return (
            <div>
                <Doughnut data={data} />
            </div>
        )
    }
}

var data = {
    datasets: [{
        data: [864, 136]
    }],
    
    // These labels appear in the legend and in the tooltips when hovering different arcs
};